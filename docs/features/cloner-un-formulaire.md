
# Cloner des formulaires existants

## Privé

Besoin de réutiliser un formulaire que vous aviez déjà fait ? Ou de gagner du temps en utilisant ou adaptant un formulaire déjà prêt à l'usage ? Vous pouvez réutiliser un formulaire en le clonant : la structure sera dupliquée automatiquement, sans qu'aucune réponse ne soit copiée.

Pour ce faire vous devez :

1. cliquer sur l'onglet **Partager**
2. cliquer sur le lien dans la section **Clonage** (`Pour cloner ce formulaire, suivez simplement ce lien :`)

Cela vous mènera à un nouveau formulaire qui sera déjà prérempli avec les champs du formulaire actuel, mais sans aucune réponse. Vous pourrez donc l'adapter à vos nouveaux besoins, sans que le formulaire courant ou ses résultats ne soient affectés.

## Publiquement
Partager vos formulaires ? Faire gagner du temps à d'autres utilisateurs ? Ou juste garder votre travail sous le coude pour la prochaine fois ? Il est possible d'enregistrer votre formulaire en tant que modèle.

Voici comment faire :

1. Cliquez sur l'onglet **Modifier** de votre formulaire Yakforms
2. Allez jusqu'à la section « **Autres options** » de la page
3. Cochez-y la case « **Lister parmi les modèles** »
4. Et ... Voilà !

![modèle de formulaire]( ../images/fonctionnalites_forms_creation_modele.gif)

NB: seule la structure sera copiée (titre, description, image éventuelle, questions et champs à remplir), **pas les réponses**.
